def hi():
    print('Welcome to myModule.py')

class Car:
    def __init__(self,catergory,fuel):
        self.catergory = catergory
        self.fuel = fuel
        return ("My car is {} type and {} powered".format(self.catergory,self.fuel))

class myCar(Car):
    def madeBy(self,name,year):
        self.name = name
        self.year = year
        return ("It was made by {} in {}".format(self.name,str(self.year)))    
    